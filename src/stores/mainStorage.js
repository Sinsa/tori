import {User} from "../entities.js";
import {writable} from "svelte/store";

const devBackend = "https://localhost:727";
const prodBackend = "tori.sinsa92.net:727";
const backend = process.env.isProd ? prodBackend : devBackend;

export const user = writable < User > null;

export const apiCall = (path, options = {headers: {}}) => {
    fetch(`${backend}${path}`, {
        ...options,
        headers: {
            Authorization: access_token ? `Bearer ${access_token}` : undefined,
            ...options.headers
        }
    })
        .then(async result => {
            const json = await result.json();
            if (json.errors[0].message === 'Token expired.') {
                const authentication = await fetch(`${backend}/auth/refresh`, {
                    method: 'POST',
                    credentials: 'include'
                });
                if (authentication.status === 200) {
                    const authenticationJson = await authentication.json();
                    access_token = authenticationJson.data.access_token;
                    return apiCall(path, options);
                }
            }
            return json;
        });

    let access_token;
}
